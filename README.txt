/**************************/
/***** Git repository *****/
/**************************/

I recommend to use this schema for our git repositry (I mean convections for branching etc):
http://nvie.com/posts/a-successful-git-branching-model/

Here you have nice introduction how to follow these convections using SourceTree application:
http://blog.sourcetreeapp.com/2012/08/01/smart-branching-with-sourcetree-and-git-flow/