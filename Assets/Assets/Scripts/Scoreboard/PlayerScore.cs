﻿//Norbert Gregorek

using System;
using Parse;

[ParseClassName("PlayerScore")]
public class PlayerScore : ParseObject
{
    [ParseFieldName("Nickname")]
    public string Nickname
    {
        get { return GetProperty<string>("Nickname"); }
        set { SetProperty<string>(value, "Nickname"); }
    }

    [ParseFieldName("Time")]
    public float Time
    {
        get { return GetProperty<float>("Time"); }
        set { SetProperty<float>(value, "Time"); }
    }
}
