﻿using UnityEngine;
using System.Collections;

public class RulesWindowManager : MonoBehaviour
{

    public void Show()
    {
        NGUITools.SetActive(this.gameObject, true);
    }

    public void Hide()
    {
        NGUITools.SetActive(this.gameObject, false);
    }
}
