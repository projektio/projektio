﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Crosshair : MonoBehaviour
{
    public Texture2D texture;
    private Rect position;

    public void Start()
    {
        position = new Rect((Screen.width - texture.width) / 2,
                            (Screen.height - texture.height) / 2,
                            texture.width, texture.height);
    }
    public void OnGUI()
    {
        GUI.DrawTexture(position, texture);
    }
}