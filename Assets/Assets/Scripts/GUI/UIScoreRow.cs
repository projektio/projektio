﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public class UIScoreRow : MonoBehaviour
{
    #region Variables
    public UILabel positionLabel;
    public UILabel nicknameLabel;
    public UILabel timeLabel;
    #endregion
}
