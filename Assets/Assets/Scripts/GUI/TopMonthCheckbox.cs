﻿using UnityEngine;
using System.Collections;

public class TopMonthCheckbox : MonoBehaviour
{
    #region Variables
    private NGUIWindowManager windowManager;
    #endregion

    #region Functions
    public void Awake()
    {
        windowManager = GameObject.FindGameObjectWithTag(Tags.gameController)
            .GetComponent<NGUIWindowManager>();
    }

    public void OnActivate(bool isActive)
    {
        if (isActive)
        {
            windowManager.ScoreBoardWindow.UpdateTop100Month();
        }
    }
    #endregion
}
