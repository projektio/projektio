﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public class ScoreWindow : MonoBehaviour
{
    #region Variables
    [SerializeField]
    private UIInput nickname;
    [SerializeField]
    private UILabel infoLabel;
    [SerializeField]
    private UILabel timeLabel;
    #endregion

    #region Properties
    public UIInput Nickname
    {
        get { return nickname; }
    }

    public UILabel InfoLabel
    {
        get { return infoLabel; }
    }

    public UILabel TimeLabel
    {
        get { return timeLabel; }
    }
    #endregion

    #region Functions
    public void Show()
    {
        Screen.showCursor = true;
        Screen.lockCursor = false;
        NGUITools.SetActive(this.gameObject, true);
    }

    public void Hide()
    {
        NGUITools.SetActive(this.gameObject, false);
    }

    public void GoToMenu()
    {
        Application.LoadLevel(0);
    }
    #endregion
}
