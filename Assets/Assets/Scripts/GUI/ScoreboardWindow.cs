﻿//Norbert Gregorek

using UnityEngine;
using System;
using System.Collections.Generic;
using Parse;

public class ScoreboardWindow : MonoBehaviour
{
    #region Variables
    [SerializeField]
    private UICheckbox top100CheckBox;
    [SerializeField]
    private UICheckbox topMonthCheckBox;
    [SerializeField]
    private UICheckbox topTodayCheckBox;
    [SerializeField]
    private UILabel infoLabel;

    private NGUIWindowManager windowManager;
    #endregion

    #region MyRegion
    //This is constant Number of showing the best results
    public const int kScoreNumber = 100;
    //This is refenrece to the scores
    private List<UIScoreRow> scoreSlotsList = new List<UIScoreRow>();
    [SerializeField]
    private GameObject scoreRowPrefab;
    //Where new 'scoreRowPrefab' will be instantiated
    [SerializeField]
    private GameObject scoreGrid;
    #endregion

    #region Functions
    public void Awake()
    {
        //Just populate 'scoreSlotsList'
        for (int i = 0; i < kScoreNumber; ++i)
        {
            UIScoreRow row = NGUITools.AddChild(scoreGrid, scoreRowPrefab).GetComponent<UIScoreRow>();
            //Setup position here
            row.positionLabel.text = (i + 1).ToString();
            scoreSlotsList.Add(row);
        }
        //Disable all scores by default
        DisableAllScores();
    }

    public void UpdateTop100()
    {
        //Update 'infoLabel'
        infoLabel.text = "Please Wait...";
        var bestScoresQuery = new ParseQuery<PlayerScore>()
            .OrderBy("Time")
            .Limit(ScoreboardWindow.kScoreNumber);

        bestScoresQuery.FindAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                IEnumerable<PlayerScore> scores = task.Result;

                Loom.QueueOnMainThread(() =>
                {
                    //And populate our results
                    PopulateScores(scores);
                    infoLabel.text = "";
                });
            }
            else
            {
                Loom.QueueOnMainThread(() =>
                {
                    infoLabel.text = "Error occured (checkout yout Internet access)";
                });
            }
        });
    }

    public void UpdateTop100Month()
    {
        DateTime startDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 1);
        DateTime endDate = startDate.AddMonths(1);

        //Update 'infoLabel'
        infoLabel.text = "Please Wait...";
        var bestScoresQuery = new ParseQuery<PlayerScore>()
            .OrderBy("Time")
            .WhereGreaterThanOrEqualTo("createdAt", startDate)
            .WhereLessThan("createdAt", endDate)
            .Limit(ScoreboardWindow.kScoreNumber);

        bestScoresQuery.FindAsync().ContinueWith(task =>
        {
            IEnumerable<PlayerScore> scores = task.Result;

            Loom.QueueOnMainThread(() =>
            {
                //And populate our results
                PopulateScores(scores);
                infoLabel.text = "";
            });
        });
    }

    public void UpdateTop100Today()
    {
        DateTime startDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day);
        DateTime endDate = startDate.AddDays(1);

        //Update 'infoLabel'
        infoLabel.text = "Please Wait...";
        var bestScoresQuery = new ParseQuery<PlayerScore>()
            .OrderBy("Time")
            .WhereGreaterThanOrEqualTo("createdAt", startDate)
            .WhereLessThan("createdAt", endDate)
            .Limit(ScoreboardWindow.kScoreNumber);

        bestScoresQuery.FindAsync().ContinueWith(task =>
        {
            IEnumerable<PlayerScore> scores = task.Result;

            Loom.QueueOnMainThread(() =>
            {
                //And populate our results
                PopulateScores(scores);
                infoLabel.text = "";
            });
        });
    }

    public void Show()
    {
        NGUITools.SetActive(this.gameObject, true);

        //Update scores depending on checkbox
        if (top100CheckBox.isChecked)
        {
            UpdateTop100();
        }
        else if (topTodayCheckBox.isChecked)
        {
            UpdateTop100Today();
        }
        else if (topMonthCheckBox.isChecked)
        {
            UpdateTop100Month();
        }
    }

    public void Hide()
    {
        NGUITools.SetActive(this.gameObject, false);
    }

    private void DisableAllScores()
    {
        foreach (UIScoreRow row in scoreSlotsList)
        {
            row.gameObject.SetActive(false);
        }
    }
    private void EnableNthScores(int n)
    {
        for (int i = 0; i < n; ++i)
        {
            scoreSlotsList[i].gameObject.SetActive(true);
        }
    }
    public void PopulateScores(IEnumerable<PlayerScore> scores)
    {
        DisableAllScores();
        
        int i = 0;
        foreach (PlayerScore score in scores)
        {
            scoreSlotsList[i].nicknameLabel.text = score.Nickname;
            scoreSlotsList[i].timeLabel.text = score.Time.ToString();
            ++i; //I forgot about it and was struggling for almost 2 hours...
        }
        EnableNthScores(i);

        UIGrid grid = scoreGrid.GetComponent<UIGrid>();
        grid.Reposition();
    }

    public void GoToMenu()
    {
        Application.LoadLevel(0);
    }
    #endregion
}