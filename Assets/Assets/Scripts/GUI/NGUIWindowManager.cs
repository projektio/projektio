﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public class NGUIWindowManager : MonoBehaviour
{
    #region Variables
    [SerializeField]
    private ScoreWindow submitScoreWindow;
    [SerializeField]
    private ScoreboardWindow scoreBoardWindow;
    [SerializeField] //We will display some info using this variable
    private UILabel infoLabel;
    [SerializeField]
    private UILabel aliveEnemiesLabel;
    [SerializeField]
    private UILabel distanceLabel;
    #endregion

    #region Properties
    public ScoreWindow SubmitScoreWindow
    {
        get { return submitScoreWindow; }
    }

    public ScoreboardWindow ScoreBoardWindow
    {
        get { return scoreBoardWindow; }
    }

    public UILabel InfoLabel
    {
        get { return infoLabel;  }
    }

    public UILabel AliveEnemiesLabel
    {
        get { return aliveEnemiesLabel;  }
    }

    public UILabel DistanceLabel
    {
        get { return distanceLabel; }
    }
    #endregion
}