﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public class Top100Checkbox : MonoBehaviour
{
    #region Variables
    private NGUIWindowManager windowManager;
    #endregion

    #region Functions
    public void Awake()
    {
        windowManager = GameObject.FindGameObjectWithTag(Tags.gameController)
            .GetComponent<NGUIWindowManager>();
    }

    public void OnActivate(bool isActive)
    {
        if (isActive)
        {
            windowManager.ScoreBoardWindow.UpdateTop100();
        }
    }
    #endregion
}
