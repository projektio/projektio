﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SkillSlot
{
    [SerializeField]
    public UISprite sprite;
    [SerializeField]
    public Skill skill;

    public void UpdateSkillSprite()
    {
        if (sprite && skill)
        {
            sprite.spriteName = skill.Thumbnail.name;
        }
    }
}
