﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms.Impl;

public class MenuWindowManager : MonoBehaviour
{
    #region Variables
    public ScoreboardWindow scoreboardWindow;
    public RulesWindowManager rulesWindow;
    public UILabel infoLabel;
    public AudioClip closeWindowSound;
    #endregion
    
    #region Functions

    void Start()
    {
        Screen.showCursor = true;
        Screen.lockCursor = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            AudioSource.PlayClipAtPoint(closeWindowSound, transform.position);
            HideWindows();
        }
    }

    public void OnRulesClicked()
    {
        HideWindows();
        rulesWindow.Show();
    }

    public void OnBestSoresClicked()
    {
        HideWindows();
        scoreboardWindow.Show();
    }

    public void OnPlayClicked()
    {
        infoLabel.text = "Please wait...";
        Screen.showCursor = false;
        Screen.lockCursor = true;
        Application.LoadLevel(Application.loadedLevel + 1);
    }

    public void OnExitClicked()
    {
        Debug.Log("EXIT");
        Application.Quit();
    }

    private void HideWindows()
    {
        scoreboardWindow.Hide();
        rulesWindow.Hide();
    }
    #endregion
}