﻿//Norbert Gregorek

using UnityEngine;
using System.Collections.Generic;
using System.Threading.Tasks;
using Parse;

public class SubmitScoreButton : MonoBehaviour
{
    #region Variables
    [SerializeField]
    private ScoreWindow window;
    private NGUIWindowManager windowManager;
    #endregion

    #region Functions
    public void Start()
    {
        windowManager = GameObject.FindGameObjectWithTag(Tags.gameController)
            .GetComponent<NGUIWindowManager>();
    }

    public void OnClick()
    {
        float time = float.Parse(window.TimeLabel.text);
        string nickname = window.Nickname.text;

        window.InfoLabel.text = "Please wait...";

        //Setup new 'PlayerScore'
        PlayerScore score = new PlayerScore();
        score.Time = time;
        score.Nickname = nickname;
        System.Threading.CancellationToken token = new System.Threading.CancellationToken();
        //Try to save it asynchronously
        score.SaveAsync(token).ContinueWith(task =>
        {
            window.InfoLabel.text = "";

            if (task.IsCompleted && token.IsCancellationRequested == false)
            {
                Loom.QueueOnMainThread(() =>
                {
                    //Close current window
                    windowManager.SubmitScoreWindow.Hide();
                    //And show Scoreboard window
                    windowManager.ScoreBoardWindow.Show();
                });
            }
            else
            {
                Loom.QueueOnMainThread(() =>
                {
                    window.InfoLabel.text = "Some error occurred";
                });
            }
        });
    }
    #endregion
}