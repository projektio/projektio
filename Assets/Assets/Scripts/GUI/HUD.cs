﻿//Norbert Gregorek

using UnityEngine;
using System.Collections.Generic;

public class HUD : MonoBehaviour
{
    #region Variables
    /*     UI REFERENCES     */
    //Health
    [SerializeField]
    private UISlider healthSlider;
    [SerializeField]
    private UILabel healthLabel;
    [SerializeField]
    private List<SkillSlot> skills;

    //Stamina
    [SerializeField]
    private UISlider staminaSlider;
    [SerializeField]
    private UILabel staminaLabel;

    //Mana
    [SerializeField]
    private UISlider manaSlider;
    [SerializeField]
    private UILabel manaLabel;

    //Current skill
    [SerializeField]
    private UISprite currentSkill;

    //Timer
    [SerializeField]
    private UILabel timeLabel;

    //Current time
    private float gameTime = 0;

    //Reference to Player object
    private Player player;

    private int skillsAdded = 0;
    #endregion

    #region Properties
    public float GameTime
    {
        get { return gameTime; }
    }

    public List<SkillSlot> Skills
    {
        get { return skills; }
    }
    #endregion

    #region Functions
    public void Awake()
    {
        player = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<Player>();

        foreach (SkillSlot slot in skills)
        {
            slot.UpdateSkillSprite();
        }
    }

    public void AddSkill(Skill skill)
    {
        Skills[skillsAdded].skill = skill;
        Skills[skillsAdded++].UpdateSkillSprite();
    }

    public void FixedUpdate()
    {
        //Refresh player's date

        //Health
        healthLabel.text = player.Stats.HealthPoints + " / " + player.Stats.MaxHealthPoints;
        healthSlider.sliderValue = (float)player.Stats.HealthPoints / player.Stats.MaxHealthPoints;

        //Stamina
        staminaLabel.text = player.Stats.StaminaPoints + " / " + player.Stats.MaxStaminaPoints;
        staminaSlider.sliderValue = (float)player.Stats.StaminaPoints / player.Stats.MaxStaminaPoints;

        //Mana
        manaLabel.text = player.Stats.ManaPoints + " / " + player.Stats.MaxManaPoints;
        manaSlider.sliderValue = (float)player.Stats.ManaPoints / player.Stats.MaxManaPoints;

        //Refresh time
        gameTime += Time.fixedDeltaTime;
        timeLabel.text = gameTime.ToString("0.0") + " (s)";

        //Skill switching

        for (int i = 0; i < skills.Count; ++i)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1 + i) && skills[i].skill != null && !player.InsideAttackState())
            {
                player.CurrentSkill = skills[i].skill;
                currentSkill.spriteName = skills[i].skill.Thumbnail.name;
                Debug.Log("Current skill " + skills[i].skill.name);
            }
        }
    }
    #endregion
}
