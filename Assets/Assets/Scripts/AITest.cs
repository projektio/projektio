﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AITest : MonoBehaviour
{
    #region Variables
    [SerializeField]
    private float attackRange;
    [SerializeField]
    private float walkingDistance;
    [SerializeField]
    private float angleAttack;
    [SerializeField]
    private float acceptablePositionChange = 1.5f;

    private Player player;
    private NavMeshAgent nav;
    private HumanoidCharacter character;
    #endregion

    public void Awake()
    {
        //Because we use sqrMagnitude
        acceptablePositionChange *= acceptablePositionChange;

        nav = GetComponent<NavMeshAgent>();
        character = GetComponent<HumanoidCharacter>();
        nav.updateRotation = false;
        player = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<Player>();
        StartCoroutine(FindDestination());
    }

    private IEnumerator FindDestination()
    {
        while (true)
        {
            if (nav == null)
            {
                yield break;
            }
            if ((nav.destination - player.transform.position).sqrMagnitude > acceptablePositionChange)
            {
                nav.destination = player.transform.position;
            }
            yield return new WaitForSeconds(1);
        }
    }

    public void Update()
    {
        if (nav == null) return;
        if (character.InsideDeathState() == false && character.InsideAttackState() == false)
        {
            if (nav.remainingDistance > nav.stoppingDistance)
            {
                if (RemaininDistance() < walkingDistance)
                    character.AnimSpeed = character.Stats.WalkingSpeed;
                else
                    character.AnimSpeed = character.Stats.RunningSpeed;
            }
            else
            {
                character.AnimSpeed = 0f;
            }
            float angle = FindAngle(transform.forward, nav.destination - transform.position, transform.up);
            character.SetAngle(angle * nav.angularSpeed);
            if (RemaininDistance() < nav.stoppingDistance && Mathf.Abs(angle) < angleAttack)
            {
                nav.Stop();
                if (player.InsideDeathState() == false)
                    character.Attack(character.PrimaryWeapon);
            }
        }
        else
        {
            nav.speed = 0f;
        }
    }

    void OnAnimatorMove()
    {
        // Set the NavMeshAgent's velocity to the change in position since the last frame, by the time it took for the last frame.
        if (nav)
            nav.velocity = character.Anim.deltaPosition / Time.deltaTime;

        // The gameobject's rotation is driven by the animation's rotation.
        transform.rotation = character.Anim.rootRotation;
    }

    float FindAngle(Vector3 fromVector, Vector3 toVector, Vector3 upVector)
    {
        // If the vector the angle is being calculated to is 0...
        if (toVector == Vector3.zero)
            // ... the angle between them is 0.
            return 0f;

        // Create a float to store the angle between the facing of the enemy and the direction it's travelling.
        float angle = Vector3.Angle(fromVector, toVector);

        // Find the cross product of the two vectors (this will point up if the velocity is to the right of forward).
        Vector3 normal = Vector3.Cross(fromVector, toVector);

        // The dot product of the normal with the upVector will be positive if they point in the same direction.
        angle *= Mathf.Sign(Vector3.Dot(normal, upVector));

        // We need to convert the angle we've found from degrees to radians.
        angle *= Mathf.Deg2Rad;

        return angle;
    }

    private float RemaininDistance()
    {
        return (transform.position - player.transform.position).magnitude;
    }
}