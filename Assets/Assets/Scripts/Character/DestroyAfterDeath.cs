﻿using UnityEngine;
using System.Collections;

public class DestroyAfterDeath : MonoBehaviour
{
    [SerializeField]
    private float seconds = 10;
    private Character character;

    public void Awake()
    {
        character = GetComponent<Character>();
    }

    public void FixedUpdate()
    {
        if (character.InsideDeathState())
        {
            seconds -= Time.deltaTime;

            if (seconds <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
