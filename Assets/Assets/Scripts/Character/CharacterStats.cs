﻿//Norbert Gregorek

using System;
using UnityEngine;
using System.Collections;

[System.Serializable]
public class CharacterStats
{
    #region Variables
    [SerializeField]
    private int team;
    //All the statistics that every character contains
    [SerializeField]
    private float walkingSpeed = 1.5f;
    [SerializeField]
    private float runningSpeed = 2f;
    [SerializeField]
    private int defensePoints = 10;
    [SerializeField]
    private int attackPoints = 10;
    [SerializeField]
    private int wizardryDamage = 10;
    [SerializeField]
    private int manaPoints = 100;
    [SerializeField]
    private int maxManaPoints = 100;
    [SerializeField]
    private int staminaPoints = 30;
    [SerializeField]
    private int maxStaminaPoints = 100;
    [SerializeField]
    private int healthPoints = 30;
    [SerializeField]
    private int maxHealthPoints = 100;

    //Each statistics is multiplied by given number
    [SerializeField]
    private float walkingSpeedMultiplier = 1;
    [SerializeField]
    private float runningSpeedMultiplier = 1;
    [SerializeField]
    private float defensePointsMultiplier = 1;
    [SerializeField]
    private float attackPointsMultiplier = 1;
    [SerializeField]
    private float manaPointsMultiplier = 1;
    [SerializeField]
    private float healthPointsMultiplier = 1;
    [SerializeField]
    private float wizardryDamageMultiplier = 1;
    [SerializeField]
    private float staminaPointsMultiplier = 1;

    private GameController gameController = null;
    #endregion

    #region Properties

    public GameController GameController
    {
        get
        {
            if (gameController == null)
            {
                gameController = GameObject.FindGameObjectWithTag(Tags.gameController)
                    .GetComponent<GameController>();
            }
            return gameController;
        }
    }
    public int Team
    {
        get { return team;  }
    }

    public Character Owner { get; set; }

    public float WalkingSpeed
    {
        get
        {
            return walkingSpeed * walkingSpeedMultiplier;
        }
        set
        {
            walkingSpeed = value;
        }
    }

    public float RunningSpeed
    {
        get
        {
            return runningSpeed * runningSpeedMultiplier;
        }
        set
        {
            runningSpeed = value;
        }
    }

    public int DefensePoints
    {
        get
        {
            return (int)(defensePoints * defensePointsMultiplier);
        }
        set
        {
            defensePoints = value;
        }
    }

    public int AttackPoints
    {
        get
        {
            return (int)(attackPoints * attackPointsMultiplier);
        }
        set
        {
            attackPoints = value;
        }
    }

    public int ManaPoints
    {
        get
        {
            return Math.Min((int)(manaPoints * manaPointsMultiplier), MaxManaPoints);
        }
        set
        {
            manaPoints = (int)(value / manaPointsMultiplier);
        }
    }

    public int MaxManaPoints
    {
        get
        {
            return (int)(maxManaPoints * manaPointsMultiplier);
        }
        set
        {
            maxManaPoints = value;
        }
    }

    public int StaminaPoints
    {
        get
        {
            return Math.Min((int)(staminaPoints * staminaPointsMultiplier), MaxStaminaPoints);
        }
        set
        {
            staminaPoints = (int)(value / staminaPointsMultiplier);
        }
    }

    public int MaxStaminaPoints
    {
        get
        {
            return (int)(maxStaminaPoints * staminaPointsMultiplier);
        }
        set
        {
            maxStaminaPoints = value;
        }
    }

    public int HealthPoints
    {
        get
        {
            return Math.Min((int)(healthPoints * healthPointsMultiplier), MaxHealthPoints);
        }
        set
        {
            //TODO it should depend on 'DefensePoints' somehow
            healthPoints = (int)(value / healthPointsMultiplier);

            if (healthPoints <= 0)
            {
                Owner.Die();
            }

            healthPoints = Mathf.Clamp(healthPoints, 0, MaxHealthPoints);
        }
    }

    public int MaxHealthPoints
    {
        get
        {
            return (int)(maxHealthPoints * healthPointsMultiplier);
        }
        set
        {
            maxHealthPoints = value;
        }
    }

    public int WizardryDamage
    {
        get
        {
            return (int)(wizardryDamage * wizardryDamageMultiplier);
        }
        set
        {
            wizardryDamage = value;
        }
    }

    /*   Multipliers   */
    public float WalkingSpeedMultiplier
    {
        get { return walkingSpeedMultiplier; }
        set { walkingSpeedMultiplier = value; }
    }

    public float RunningSpeedMultiplier
    {
        get { return runningSpeedMultiplier; }
        set { runningSpeedMultiplier = value; }
    }

    public float DefensePointsMultiplier
    {
        get { return defensePointsMultiplier; }
        set { defensePointsMultiplier = value; }
    }

    public float AttackPointsMultiplier
    {
        get { return attackPointsMultiplier; }
        set { attackPointsMultiplier = value; }
    }

    public float ManaPointsMultiplier
    {
        get { return manaPointsMultiplier; }
        set { manaPointsMultiplier = value; }
    }

    public float WizardryDamageMultiplier
    {
        get { return wizardryDamageMultiplier; }
        set { wizardryDamageMultiplier = value; }
    }
    #endregion

    #region Methods
    //TODO it should change a little bit
    public void TakeDamage(CharacterStats stats)
    {
        if (team != stats.team)
        {
            HealthPoints -= stats.attackPoints;
        }
    }

    public void TakeDamage(CharacterStats stats, Vector3 hitPosition)
    {
        if (team != stats.team)
        {
            HealthPoints -= stats.attackPoints;
            GameObject.Instantiate(GameController.BloodEffect, hitPosition, Quaternion.identity);
        }
    }
    #endregion
}