﻿using UnityEngine;
using System.Collections;

public class CharacterSounds : MonoBehaviour
{
    #region Variables

    public AudioClip RunClip;
    public AudioClip DeathClip;

    #endregion
}
