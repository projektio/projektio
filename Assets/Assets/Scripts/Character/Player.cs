﻿//Norbert Gregorek

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;


public class Player : HumanoidCharacter
{
    [SerializeField]
    private float turnSmoothing = 1f;
    [SerializeField]
    private float runSmoothing = 1f;
    [SerializeField]
    private float waitUntilGoToMenuTime = 3;
	private Transform cameraTransform;
    private int currentWeaponIndex = -1;
    private bool usedQ;
    private bool usedE;

    public List<Weapon> weaponList;
    public List<Skill> skillList;

    public override void Awake()
    {
        base.Awake();
		cameraTransform = GameObject.FindGameObjectWithTag(Tags.mainCamera).transform;
        usedQ = usedE = false;
    }

    private IEnumerator GoToMenu()
    {
        yield return new WaitForSeconds(waitUntilGoToMenuTime);
        Application.LoadLevel(0);
    }

    public void FixedUpdate ()
    {
        if (InsideDeathState())
        {
            StartCoroutine(GoToMenu());
        }
        else
        {
            //Get the input 
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");
            MovementManagement(h, v);
        }
    }
    
    void MovementManagement(float h, float v)
    {
        if (InsideDeathState())
            return;
        if (h != 0f || v != 0f)
        {
            RotatePlayer(h, v);
            if (InsideLocomotionState())
            {
                float speed = Stats.WalkingSpeed;
                if (Input.GetKey(KeyCode.LeftShift) == true)
                {
                    speed = Stats.RunningSpeed;
                }
                speed = Mathf.Lerp(AnimSpeed, speed, runSmoothing * Time.deltaTime);
                AnimSpeed = Mathf.Max(Mathf.Abs(h), Mathf.Abs(v)) * speed;
            }
        }
        else //There is no movement (speed should be equal 0)
        {
            AnimSpeed = 0;
        }

        //Next weapon
        if (Input.GetKeyDown(KeyCode.Q) && !InsideAttackState())
        {
            if (!usedQ)
            {
                usedQ = true;
                if (currentWeaponIndex == -1 && weaponList.Count > 0)
                {
                    currentWeaponIndex = 0;
                    PrimaryWeapon = weaponList[currentWeaponIndex];
                    PrimaryWeapon.gameObject.SetActive(true);
                }
                else if (weaponList.Count > 0)
                {
                    //Disable previous one
                    if (PrimaryWeapon != null)
                    {
                        PrimaryWeapon.gameObject.SetActive(false);
                    }
                    currentWeaponIndex = (currentWeaponIndex + 1) % weaponList.Count;
                    PrimaryWeapon = weaponList[currentWeaponIndex];
                    PrimaryWeapon.gameObject.SetActive(true);
                }
            }
        }
        else
        {
            usedQ = false;
        }

        //Previous weapon
        if (Input.GetKeyDown(KeyCode.E) && !InsideAttackState())
        {
            if (!usedE)
            {
                usedE = true;
                if (currentWeaponIndex == -1 && weaponList.Count > 0)
                {
                    currentWeaponIndex = 0;
                    PrimaryWeapon = weaponList[currentWeaponIndex];
                    PrimaryWeapon.gameObject.SetActive(true);
                }
                else if (weaponList.Count > 0)
                {
                    //Disable previous one
                    if (PrimaryWeapon != null)
                    {
                        PrimaryWeapon.gameObject.SetActive(false);
                    }
                    currentWeaponIndex = (currentWeaponIndex - 1 + weaponList.Count) % weaponList.Count;
                    PrimaryWeapon = weaponList[currentWeaponIndex];
                    PrimaryWeapon.gameObject.SetActive(true);
                }
            }
        } 
        else
        {
            usedE = false;
        }

        if (Input.GetMouseButtonDown(0) && !InsideAttackState() && PrimaryWeapon)
        {
            Attack(PrimaryWeapon);
        }

        if (Input.GetMouseButtonDown(1) && !InsideAttackState() && CurrentSkill && CurrentSkill.CanUse())
        {
            Attack(CurrentSkill);
        }
    }

    void RotatePlayer(float h, float v)
    {
		Vector3 playerForward = cameraTransform.forward;
		playerForward[1] = transform.forward[1];
        // Create a new vector of the horizontal and vertical inputs.

		Vector3 targetDirection = Quaternion.LookRotation(playerForward, Vector3.up) * new Vector3(h, 0f, v);
        
        // Create a rotation based on this new vector assuming that up is the global y axis.

		Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
        
        // Create a rotation that is an increment closer to the target rotation from the player's rotation.

        Quaternion newRotation = Quaternion.Lerp(transform.rotation, targetRotation, turnSmoothing * Time.deltaTime);
        
        // Change the players rotation to this new rotation.

        transform.rotation = newRotation;
    }
}