﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : MonoBehaviour
{
    #region Variables
    [SerializeField]
    private CharacterStats stats = null;
    [SerializeField]
    private CharacterType characterType;

    [SerializeField] //Primary
    protected Transform primaryWeaponTransform = null;
    [SerializeField]
    protected Weapon primaryWeapon = null;

    /*   Skill   */
    [SerializeField] //A place where we can instantiate different skill effects (for instance "Fire ball")
    protected Transform skillTransform;
    [SerializeField]
    protected Skill currentSkill = null;

    /*   Animations   */
    private AnimatorHashIDs hashIDs;
    [SerializeField]
    private DeathAnimationEnum deathAnimation;
    private int deathAnimationHash;

    /*  Combat system   */
    private List<Character> attacked;

    /* Sounds */
    protected CharacterSounds soundEffects;
    #endregion

    #region Properties

    public Weapon PrimaryWeapon
    {
        get { return primaryWeapon; }
        set
        {
            Equip(value, primaryWeaponTransform, PrimaryWeapon);
            primaryWeapon = value;
        }
    }

    public Animator Anim { get; set; }
    public CharacterStats Stats
    {
        get { return stats; }
    }

    public CharacterType CharacterType
    {
        get { return characterType; }
    }

    public Skill CurrentSkill
    {
        get { return currentSkill; }
        set
        {
            Equip(value, transform, CurrentSkill);
            currentSkill = value;
        }
    }

    public Transform SkillTransform
    {
        get { return skillTransform; }
    }

    /*   Animations   */
    public float AnimSpeed
    {
        get { return Anim.GetFloat("Speed"); }
        set { Anim.SetFloat("Speed", value, 0.1f, Time.deltaTime); }
    }

    public int DeathAnimationHash
    {
        get { return deathAnimationHash; }
    }
    #endregion

    #region Methods
    public virtual void Awake()
    {
        Anim = GetComponent<Animator>();
        hashIDs = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<AnimatorHashIDs>();
        Stats.Owner = this;
        deathAnimationHash = Animator.StringToHash(deathAnimation.ToString());
        soundEffects = GetComponent<CharacterSounds>();
        attacked = new List<Character>();

    }
    /* Equip our character with 'item'.
     * return true if we can equip this item, oterwise false. */
    public bool Equip(Item item, Transform transform, Item previousItem)
    {
        if (item.CanEquip(CharacterType))
        {
            //Place the new item as a child of 'transform'
            Vector3 tmpPosition = item.transform.localPosition;
            Quaternion tmpRotation = item.transform.localRotation;

            item.transform.parent = transform;

            item.transform.localPosition = tmpPosition;
            item.transform.localRotation = tmpRotation;
            if (previousItem != null)
            {
                previousItem.OnRemoveItem();
            }
            //And change the owner of this item
            item.Owner = this;
            item.OnEquipItem();
            return true;
        }
        else
        {
            return false;
        }
    }

    

    public void SetAngle(float angle)
    {
        Anim.SetFloat("Angular Speed", angle, 0.3f, Time.deltaTime);
    }

    public void Die()
    {
        Anim.CrossFade(deathAnimationHash, 0.2f);

        //Destroy collider after Die
        Collider collider = GetComponent<Collider>();
        if (collider)
        {
            Destroy(collider);
        }

        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        if (agent)
        {
            Destroy(agent);
        }

        //Inform game controller that a character has died
        GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<GameController>()
            .OnCharacterDead(this);

        if (soundEffects && soundEffects.DeathClip)
        {
            AudioSource.PlayClipAtPoint(soundEffects.DeathClip, transform.position);
        }
    }

    public bool InsideLocomotionState()
    {
        return Anim.GetCurrentAnimatorStateInfo(0).nameHash == hashIDs.LocomotionState;
    }

    public bool InsideAttackState()
    {
        return Anim.GetCurrentAnimatorStateInfo(0).IsTag(Tags.attackAnimation) ||
            Anim.GetNextAnimatorStateInfo(0).IsTag(Tags.attackAnimation);
    }

    public bool InsideSkillState()
    {
        return Anim.GetCurrentAnimatorStateInfo(0).IsTag(Tags.skillAnimation) ||
            Anim.GetNextAnimatorStateInfo(0).IsTag(Tags.skillAnimation);
    }

    public bool InsideDeathState()
    {
        return Anim.GetCurrentAnimatorStateInfo(0).IsTag(Tags.deathAnimation);
    }

    public void Attack(Weapon weapon)
    {
        if (weapon && InsideDeathState() == false && InsideAttackState() == false)
        {
            attacked.Clear();
            Anim.CrossFade(weapon.AnimationHash, 0.2f);
        }
    }

    public void Attack(Skill skill)
    {
		if (InsideDeathState() == false && InsideAttackState() == false &&
            InsideSkillState() == false &&
            skill && stats.ManaPoints >= skill.RequiredMana)
        {
            attacked.Clear();
            stats.ManaPoints -= skill.RequiredMana;
            Anim.CrossFade(skill.AnimationHash, 0.2f);
        }
    }

    public bool Attacked(Character other)
    {
        if (attacked.Contains(other))
            return false;
        attacked.Add(other);
        return true;
    }
    #endregion
}