﻿using UnityEngine;
using System.Collections;

public class HumanoidCharacter : Character
{
    #region Variables
    /*   Weapons   */
    [SerializeField] //Auxiliary
    protected Transform auxiliaryWeaponTransform;
    [SerializeField]
    protected Weapon auxiliaryWeapon = null;

    public float RunClipThreshold = 0.05f;
    #endregion

    #region Properties

    public Transform PrimaryWeaponTransform
    {
        get { return primaryWeaponTransform; }
    }

    public Weapon AuxiliaryWeapon
    {
        get { return auxiliaryWeapon; }
        set
        {
            Equip(value, auxiliaryWeaponTransform, AuxiliaryWeapon);
            auxiliaryWeapon = value;
        }
    }

    public Transform AuxiliaryWeaponTransform
    {
        get { return auxiliaryWeaponTransform; }
    }
    #endregion

    #region Methods
    //Weapon animation events
    private void OnBeginPrimaryWeaponAnimation()
    {
        primaryWeapon.OnBeginAnimation();
    }

    private void OnEndPrimaryWeaponAnimation()
    {
        primaryWeapon.OnEndAnimation();
    }

    private void OnInsidePrimaryWeaponAnimation(string version)
    {
        if (version == "WeaponUsageSound" && primaryWeapon.WeaponUsageClip)
        {
            AudioSource.PlayClipAtPoint(primaryWeapon.WeaponUsageClip, transform.position);
        }
        else
        {
            primaryWeapon.OnInsideAnimation(version);
        }
    }

    private void OnInsideAnimation(string version)
    {
        if (version == "FootHit" && soundEffects && soundEffects.RunClip && AnimSpeed > RunClipThreshold)
        {
            AudioSource.PlayClipAtPoint(soundEffects.RunClip, transform.position);
        }
    }

    //Skill animation events
    private void OnBeginSkillAnimation()
    {
        currentSkill.OnBeginAnimation();
    }

    private void OnEndSkillAnimation()
    {
        currentSkill.OnEndAnimation();
    }

    private void OnInsideSkillAnimation(string version)
    {
        Debug.Log(version);
        if (version == "WeaponUsageSound" && currentSkill.WeaponUsageClip)
        {
            AudioSource.PlayClipAtPoint(currentSkill.WeaponUsageClip, transform.position);
        }
        else
        {
            currentSkill.OnInsideAnimation(version);
        }
    }

    public override void Awake()
    {
        base.Awake();
        //Right hand
        if (primaryWeaponTransform == null)
        {
            primaryWeaponTransform = SearchHierarchyForObject(transform, "RightHand");
            if (primaryWeaponTransform == null)
            {
                Debug.Log("Cannot find RightHand transform");
            }
        }
        //Skill transform
        if (skillTransform == null)
        {
            skillTransform = SearchHierarchyForObject(transform, "SkillTransform");
            if (skillTransform == null)
            {
                Debug.Log("Cannot find SkillTransform");
            }
        }

        if (primaryWeaponTransform != null && primaryWeapon != null)
        {
            PrimaryWeapon = (Weapon) Instantiate(primaryWeapon);
        }
    }

    //Auxiliary function to find appropriate object
    public static Transform SearchHierarchyForObject(Transform current, string name)
    {
        if (current.name == name)
        {
            return current;
        }
        else
        {
            for (int i = 0; i < current.childCount; ++i)
            {
                Transform found = SearchHierarchyForObject(current.GetChild(i), name);

                if (found != null)
                {
                    return found;
                }
            }
        }
        return null;
    }
    #endregion
}