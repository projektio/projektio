﻿//Norbert Gregorek

public enum CharacterType
{
    ANY, //It means combination of all characters (we can use it regardless of class)
    WARRIOR,
    WIZZARD,
    ARCHER
}