﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public class AnimatorControllerManager : MonoBehaviour
{
    private Animator anim;
    private AnimatorHashIDs hashIDs;
    private Character character;

    void Awake()
    {
        //We assume that there is anim controller in this GameObject
        anim = GetComponent<Animator>();
        character = GetComponent<Character>();
        hashIDs = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<AnimatorHashIDs>();

        if (anim == null)
        {
            Debug.Log("Animator Controller is missed");
        }
    }

    public void Die()
    {
        anim.CrossFade(character.DeathAnimationHash, 0.2f);
    }

    public void SetSpeed(float value)
    {
        anim.SetFloat(hashIDs.Speed, value);
    }

    public void SetAngle(float value)
    {
        anim.SetFloat(hashIDs.AngularSpeed, value);
    }

    public void Attack(Weapon weapon)
    {
        anim.CrossFade(weapon.AnimationHash, 0.2f);
    }


    //Getters to our states
    public bool InsideLocomotionState()
    {
        return anim.GetCurrentAnimatorStateInfo(0).nameHash == hashIDs.LocomotionState;
    }
    public bool InsideAttackState()
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsTag(Tags.attackAnimation);
    }
}