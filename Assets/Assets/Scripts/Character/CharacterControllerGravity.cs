﻿//Norbert Gregorek
using UnityEngine;
using System.Collections;

public class CharacterControllerGravity : MonoBehaviour
{
    [SerializeField]
    private float gravity = 9.8f;

    private CharacterController characterController;

    public void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }

    public void Update()
    {
        if (characterController)
        {
            characterController.Move(new Vector3(0, -gravity * Time.deltaTime, 0));
        }
    }
}
