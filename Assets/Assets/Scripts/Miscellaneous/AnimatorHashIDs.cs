﻿//Norbert Gregorek
using UnityEngine;
using System.Collections;

public class AnimatorHashIDs : MonoBehaviour
{
    //Animation state hashes
    public int LocomotionState { get; private set; }
    
    //Locomotion hashes
    public int Speed { get; private set; }
    public int AngularSpeed { get; private set; }

    //Attack hashes
    public int SwordSkill_1 { get; private set; }

    void Awake()
    {
        LocomotionState = Animator.StringToHash("Base Layer.Locomotion");
        Speed = Animator.StringToHash("Speed");
        AngularSpeed = Animator.StringToHash("Angular Speed");
    }
}
