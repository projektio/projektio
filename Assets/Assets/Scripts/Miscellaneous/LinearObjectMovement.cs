﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public class LinearObjectMovement : MonoBehaviour
{
    [SerializeField]
    private float speed;

    public void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }
}
