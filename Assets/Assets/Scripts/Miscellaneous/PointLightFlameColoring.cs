﻿using UnityEngine;
using System.Collections.Generic;

//Norbert Gregorek
//Instead of this script some shader should be written!!!
//But I don't have time right now

public class PointLightFlameColoring : MonoBehaviour
{
    #region Variables
    [SerializeField]
    private List<Color> colorList;
    [SerializeField]
    private Light pointLight;
    [SerializeField]
    private float durationTime = 1;

    private Color currentColor;
    private Color nextColor;
    private int colorIndex;
    private float changeColorTime;
    private float deltaTime;
    #endregion

    #region Functions

    void Awake()
    {
        colorIndex = 0;
        currentColor = colorList[0];
        nextColor = colorList[1];
        changeColorTime = durationTime / colorList.Count;
    }

    void FixedUpdate()
    {
        deltaTime += Time.deltaTime;
        if (deltaTime >= changeColorTime)
        {
            ChangeColors();
        }
        pointLight.color = Color.Lerp(currentColor, nextColor, deltaTime/changeColorTime);
    }

    private void ChangeColors()
    {
        Debug.Log(colorIndex);
        currentColor = nextColor;
        colorIndex = (colorIndex + 1) % colorList.Count;
        nextColor = colorList[colorIndex];
        deltaTime = 0;
    }
    #endregion
}
