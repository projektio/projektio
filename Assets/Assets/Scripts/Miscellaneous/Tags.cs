﻿using UnityEngine;
using System.Collections;

public class Tags
{
    public const string player = "CharacterPlayer";
    public const string character = "Character";
    public const string gameController = "GameController";
	public const string mainCamera = "MainCamera";
    public const string attackAnimation = "AttackAnimation";
    public const string skillAnimation = "SkillAnimation";
    public const string deathAnimation = "DeathAnimation";
    public const string item = "Item";
    public const string edge = "Edge";
    public const string graphController = "GraphController";
    public const string terrain = "Terrain";
}