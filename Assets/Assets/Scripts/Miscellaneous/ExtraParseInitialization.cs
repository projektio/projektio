﻿//Norbert Gregorek

using UnityEngine;
using Parse;

public class ExtraParseInitialization : MonoBehaviour
{
    void Awake()
    {
        ParseObject.RegisterSubclass<PlayerScore>();
    }
}