﻿//Norbert Gregorek

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Extension
{
    static class ExtendedMonoBehaviour
    {
        //Component extensions
        public static T GetInterface<T>(this Component inObj) where T : class
        {
            if (!typeof(T).IsInterface)
            {
                Debug.LogError(typeof(T).ToString() + ": is not an actual interface!");
                return null;
            }

            return inObj.GetComponents<Component>().OfType<T>().FirstOrDefault();
        }

        public static IEnumerable<T> GetInterfaces<T>(this Component inObj) where T : class
        {
            if (!typeof(T).IsInterface)
            {
                Debug.LogError(typeof(T).ToString() + ": is not an actual interface!");
                return Enumerable.Empty<T>();
            }

            return inObj.GetComponents<Component>().OfType<T>();
        }
    }
}