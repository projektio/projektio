﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public Transform target;

    public float mouseSensitivityX = 15f;
    public float mouseSensitivityY = 15f;
    public float scrollMaxSpeed = 300f;
    public float scrollAcceleration = 600f;
    public float scrollDeclerationTime = 0.5f;
    public float minimumZoom = 0.5f;
    public float maximumZoom = 4.0f;   

    public float minimumXAngle = -360f;
    public float maximumXAngle = 360;
    public float minimumYAngle = -360;
    public float maximumYAngle = 360;

    //The relative position of the camera
    private Vector3 relCameraPos;

    //Euler's angles
    private float x;
    private float y;
    private float relXrotation;
    private Player player;


    //
    private float scrollSpeed;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<Player>();
        relCameraPos = transform.position - target.position;
        Vector3 eulerAngles = transform.eulerAngles;

        relXrotation = transform.localEulerAngles.x;
        x = eulerAngles.x;
        y = eulerAngles.y;
    }

    void LateUpdate()
    {
        scrollSpeed = scrollSpeed + scrollAcceleration * Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime;
        scrollSpeed = Mathf.Clamp(scrollSpeed, -scrollMaxSpeed, scrollMaxSpeed);
        relCameraPos.z += scrollSpeed * Time.deltaTime;
        relCameraPos.z = Mathf.Clamp(relCameraPos.z, -maximumZoom, -minimumZoom);
        scrollSpeed -= scrollSpeed * Time.deltaTime / scrollDeclerationTime;

        x += Input.GetAxis("Mouse X") * mouseSensitivityX * Time.deltaTime;
        y -= Input.GetAxis("Mouse Y") * mouseSensitivityY * Time.deltaTime;
        y = Mathf.Clamp(y, minimumYAngle, maximumYAngle);

        Quaternion rotation = Quaternion.Euler(y, x, 0);
        transform.rotation = rotation;
        Vector3 position = (rotation * relCameraPos) + target.position;

        Vector3 tmpRotation = player.SkillTransform.localEulerAngles;
        tmpRotation.x = transform.eulerAngles.x - relXrotation;

        player.SkillTransform.localEulerAngles = tmpRotation;

        

        transform.position = position;
    }
}