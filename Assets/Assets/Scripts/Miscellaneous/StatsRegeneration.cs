﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

//Norbert Gregorek

public class StatsRegeneration : MonoBehaviour
{
    #region Variables
    enum RegenerationType
    {
        HEALTH,
        STAMINA,
        MANA
    }

    [System.Serializable]
    class RegenerationStruct
    {
        public RegenerationType type;
        public float timeToRegenerate;
    }

    [SerializeField]
    private List<RegenerationStruct> regenerationTypes;
    [SerializeField]
    private float regenerationStepTime = 1;
    private CharacterStats stats;
    #endregion

    #region Functions
    void Awake()
    {
        stats = GetComponent<Character>().Stats;
        foreach (var regenerationPropety in regenerationTypes)
        {
            StartCoroutine(RegenerateProperty(regenerationPropety));
        }
    }

    private IEnumerator RegenerateProperty(RegenerationStruct property)
    {
        float val = 0;
        while (true)
        {
            if (stats == null)
            {
                yield break;
            }
            yield return new WaitForSeconds(regenerationStepTime);
            switch (property.type)
            {
                case RegenerationType.HEALTH:
                    val += stats.MaxHealthPoints*(regenerationStepTime / property.timeToRegenerate);
                    stats.HealthPoints += (int) val;
                    val -= (int) val;
                    break;
                case RegenerationType.STAMINA:
                    val += stats.MaxStaminaPoints * (regenerationStepTime / property.timeToRegenerate);
                    stats.StaminaPoints += (int)val;
                    val -= (int)val;
                    break;
                case RegenerationType.MANA:
                    val += stats.MaxManaPoints * (regenerationStepTime / property.timeToRegenerate);
                    stats.ManaPoints += (int)val;
                    val -= (int)val;
                    break;
            }
        }
    }
    #endregion
}