﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour
{
    //The time after this object will be destroyed in seconds
    [SerializeField]
    private float time;

    public void Awake()
    {
        Destroy(this.gameObject, time);
    }
}
