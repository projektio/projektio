﻿//Norbert Gregorek

using UnityEngine;
using System.Collections.Generic;
using Graph;

public class GameController : MonoBehaviour
{
    #region Variables
    [SerializeField]
    public Vector3 nextEnemyPosDelta;
    [SerializeField]
    private List<GameObject> enemyList;
    [SerializeField]
    private GameObject bloodEffect;

    private int aliveCharacters = 0;
    private Edge currentEdge = null;
    private Player player;
    private HUD hud;
    private NGUIWindowManager nguiWindowManager;
    #endregion

    #region Properties
    public GameObject BloodEffect
    {
        get { return bloodEffect; }
    }
    #endregion

    #region Functions
    void Start()
    {
        if (GameObject.FindGameObjectWithTag(Tags.player) != null)
            player = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<Player>();

        hud = GetComponent<HUD>();
        nguiWindowManager = GetComponent<NGUIWindowManager>();

        for (int i = 0; i < player.skillList.Count; ++i)
        {
            hud.AddSkill(player.skillList[i]);
        }
    }

    public void PopulateEnemies(Edge edge)
    {
        currentEdge = edge;
        Vector3 point = edge.DestinationNode.transform.position;
        point.y = Terrain.activeTerrain.SampleHeight(point)
            + Terrain.activeTerrain.GetPosition().y;
        Instantiate(enemyList[0], point, Quaternion.identity);
        ++aliveCharacters;

        int enemyCount = edge.DestinationNode.Depth;

        if (edge.DestinationNode.Depth >= 3 && edge.DestinationNode.Depth < 6)
        {
            enemyCount *= 2;
        }
        else if (edge.DestinationNode.Depth >= 6)
        {
            enemyCount *= 3;
        }

        for (int i = 1; i <= enemyCount; ++i)
        {

            point = edge.DestinationNode.transform.position;
            point.y = Terrain.activeTerrain.SampleHeight(point)
                + Terrain.activeTerrain.GetPosition().y;
            Instantiate(enemyList[0], point, Quaternion.identity);
            point += nextEnemyPosDelta;

            ++aliveCharacters;
        }
        if (edge.DestinationNode.GetComponent<Leaf>() != null)
        {
            //boss
            point.y = Terrain.activeTerrain.SampleHeight(point)
                + Terrain.activeTerrain.GetPosition().y;
            Instantiate(enemyList[1], point, Quaternion.identity);
            ++aliveCharacters;
        }
        nguiWindowManager.AliveEnemiesLabel.text = "Alive enemies: " + aliveCharacters;
    }

    public void OnCharacterDead(Character character)
    {
        if (--aliveCharacters == 0)
        {
            GameObject.FindGameObjectWithTag(Tags.graphController)
                .GetComponent<GraphManager>().OnEdgeAccomplish(currentEdge);
        }
        nguiWindowManager.AliveEnemiesLabel.text = "Alive enemies: " + aliveCharacters;
    }

    public void AddWeaponToPlayer(Weapon weapon)
    {
        player.weaponList.Add(weapon);
    }

    public void AddWeaponToPlayer(Skill skill)
    {
        player.skillList.Add(skill);
        hud.AddSkill(skill);
    }
    #endregion
}