﻿using UnityEngine;
using System.Collections;

public class Sword : Weapon
{
    #region Variables
    [SerializeField]
    private int damagePoints;

    private GameController gameController;
    #endregion

    #region Properties
    public int DamagePoints
    {
        get
        {
            return damagePoints;
        }
    }
    #endregion

    #region Methods
    public override void Awake()
    {
        base.Awake();
        gameController = GameObject.FindGameObjectWithTag(Tags.gameController)
            .GetComponent<GameController>();
    }
    //Methods from Item
    public override void OnEquipItem()
    {
        if (Owner != null)
        {
            Owner.Stats.AttackPoints += DamagePoints;
        }
    }
    public override void OnRemoveItem()
    {
        if (Owner != null)
        {
            Owner.Stats.AttackPoints -= DamagePoints;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (Owner.InsideAttackState() && (other.gameObject.tag == Tags.character || other.gameObject.tag == Tags.player))
        {
            Character character = other.gameObject.GetComponent<Character>();
            if (character != Owner && Owner.Attacked(character))
            {
                character.Stats.TakeDamage(Owner.Stats, collider.transform.position);
            }
        }
    }
    #endregion
}
