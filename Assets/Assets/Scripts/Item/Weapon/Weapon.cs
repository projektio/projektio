﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public class Weapon : Item
{
    #region Variables
    [SerializeField]
    private string animationName;
    private WeaponAnimationEvents animationEvents = null;

    //For performance reasons we hold a hash of the attackAnimationName variable.
    private int animationHash;

    public AudioClip WeaponUsageClip;
    #endregion

    #region Properties
    public int AnimationHash
    {
        get
        {
            return animationHash;
        }
    }

    public override Character Owner
    {
        get { return base.Owner; }
        set
        {
            base.Owner = value;
            if (animationEvents != null)
            {
                animationEvents.Owner = value;
            }
        }
    }
    #endregion

    #region Methods
    public virtual void Awake()
    {
        animationEvents = GetComponent<WeaponAnimationEvents>();
        animationHash = Animator.StringToHash(animationName);
    }

    //Implements all functions from 'animationEvents' for convenience.
    public void OnBeginAnimation()
    {
        if (animationEvents != null)
        {
            animationEvents.OnBeginAnimation();
        }
    }

    public void OnEndAnimation()
    {
        if (animationEvents != null)
        {
            animationEvents.OnEndAnimation();
        }
    }

    public void OnInsideAnimation(string version)
    {
        if (animationEvents != null)
        {
            animationEvents.OnInsideAnimation(version);
        }
    }
    #endregion
}