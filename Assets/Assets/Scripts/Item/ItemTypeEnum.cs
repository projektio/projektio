﻿//Norbert Gregorek
using UnityEngine;

public enum ItemType
{
    PRIMARY_WEAPON,
    AUXILIARY_WEAPON,
    WARRIOR_SKILL
}