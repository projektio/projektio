﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Item : MonoBehaviour
{
    #region Variables
    [SerializeField]
    public Texture2D thumbnail;
    [SerializeField]
    protected ItemType itemType;
    [SerializeField] //Which characters can equip this item
    protected List<CharacterType> availableCharacterTypes;
    [SerializeField]
    private string name;
    #endregion

    #region Properties
    public Texture2D Thumbnail
    {
        get { return thumbnail; }
    }

    public ItemType ItemType
    {
        get { return itemType; }
    }

    public string Name
    {
        get { return name; }
    }

    public virtual Character Owner { get; set; }
    #endregion

    #region Methods
    //Check whether characterType can equip this item
    public bool CanEquip(CharacterType characterType)
    {
        foreach (CharacterType type in availableCharacterTypes)
        {
            if (type == CharacterType.ANY || type == characterType)
            {
                return true;
            }
        }
        return false;
    }

    //It's called when a character calls Equip method
    public virtual void OnEquipItem()
    {
    }

    /* It's called when a character Equip some item but the previous reference
     * is not null, then the previous object will call OnRemoveItem
     * for instace when player change Sword then we decrease damage from player stats. */
    public virtual void OnRemoveItem()
    {
    }

    #endregion
}