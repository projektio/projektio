﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

[System.Serializable]
public class Skill : Weapon
{
    private enum SkillType
    {
        SWORD,
        BOW,
        ANY
    };
    #region Variables
    [SerializeField]
    private int requiredMana;

    [SerializeField] private SkillType skillType;
    #endregion

    #region Properties
    public int RequiredMana
    {
        get { return requiredMana; }
    }
    #endregion

    #region Methods
    public override void OnEquipItem() { }
    public override void OnRemoveItem() { }

    public bool CanUse()
    {
        switch (skillType)
        {
            case SkillType.ANY:
                return true;
            case SkillType.BOW:
                return Owner.PrimaryWeapon != null && (Owner.PrimaryWeapon is Bow);
            case SkillType.SWORD:
                return Owner.PrimaryWeapon != null && (Owner.PrimaryWeapon is Sword);
            default:
                return false;
        }
    }
    #endregion
}