﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public class FlyingObjectAnimationEvents : WeaponAnimationEvents
{
    enum STARTING_TRANSFORM { INHERIT_SKILL_OWNER_TRANSFORM, USE_OWN_TRANSFORM }
    #region Variables
    [SerializeField]
    private FlyingWeaponObject flyingObject; //Object we're going to instantiate

    [SerializeField]
    private Transform originTransform;

    [SerializeField]
    private STARTING_TRANSFORM transformType = STARTING_TRANSFORM.INHERIT_SKILL_OWNER_TRANSFORM;
    #endregion

    #region Methods

    public override void OnBeginAnimation() { }

    public override void OnInsideAnimation(string version)
    {
        if (transformType == STARTING_TRANSFORM.INHERIT_SKILL_OWNER_TRANSFORM)
        {
            originTransform = Owner.SkillTransform;
        }
        FlyingWeaponObject clone = Instantiate(flyingObject, originTransform.position, originTransform.rotation) as FlyingWeaponObject;
        Physics.IgnoreCollision(clone.collider, originTransform.root.collider);
        clone.Owner = Owner;
        //Add force to this object
    }
    public override void OnEndAnimation() { }
    #endregion
}