﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public abstract class WeaponAnimationEvents : MonoBehaviour
{
    #region Properties
    public Character Owner { get; set; }
    #endregion

    #region Methods
    public abstract void OnBeginAnimation();
    public abstract void OnEndAnimation();
    public abstract void OnInsideAnimation(string version);
    #endregion
}
