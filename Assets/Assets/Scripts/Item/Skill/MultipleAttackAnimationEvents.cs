﻿using UnityEngine;
using System.Collections;

public class MultipleAttackAnimationEvents : WeaponAnimationEvents
{
    #region Methods
    public override void OnBeginAnimation() { }

    public override void OnInsideAnimation(string version) { }
    public override void OnEndAnimation() { }
    #endregion
}
