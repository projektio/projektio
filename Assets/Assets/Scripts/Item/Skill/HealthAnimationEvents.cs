﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public class HealthAnimationEvents : WeaponAnimationEvents
{
    enum HealthType { VALUE, PERCENTAGE }

    #region Variables
    [SerializeField]
    private HealthType type = HealthType.VALUE;
    [SerializeField]
    private int health;
    [SerializeField]
    private GameObject healthEffect;
    #endregion

    #region Methods
    public override void OnBeginAnimation() { }

    public override void OnInsideAnimation(string version)
    {
        Debug.Log(Owner);
        //Instantiate a new 'healthEffect'
        Instantiate(healthEffect, Owner.transform.position, Owner.transform.rotation);
        switch (type)
        {
            case HealthType.VALUE:
                Owner.Stats.HealthPoints += health;
                break;
            case HealthType.PERCENTAGE:
                Owner.Stats.HealthPoints += (int)(health / 100.0f * Owner.Stats.MaxHealthPoints);
                break;
        }
    }
    public override void OnEndAnimation() { }
    #endregion
}