﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public class HellFireAnimationEvents : WeaponAnimationEvents
{
    #region Variables
    [SerializeField]
    //It must contain sphere collider!!
    private GameObject hellFireExplosion;
    private SphereCollider hellFireCollider;
    #endregion

    #region Methods
    public void Awake()
    {
        hellFireCollider = hellFireExplosion.GetComponent<SphereCollider>();
    }

    public override void OnBeginAnimation() { }
    public override void OnInsideAnimation(string version)
    {
        if (version == "hitGround")
        {
            //Instantiate 'hellFireExplosion'
            Instantiate(hellFireExplosion, Owner.transform.position + new Vector3(0,1f,0), Owner.transform.rotation);

            float hellFireRadius = hellFireCollider.radius;

            Collider[] colliders = Physics.OverlapSphere(Owner.transform.position, hellFireRadius);

            foreach (Collider collider in colliders)
            {
                if (collider.gameObject.tag.StartsWith(Tags.character)
                    && collider.gameObject.tag != Tags.player)
                {
                    Character character = collider.gameObject.GetComponent<Character>();

                    //TODO Maybe it should be WizzardDamage ?
                    character.Stats.TakeDamage(Owner.Stats);
                }
            }
        }
        else
        {
            Debug.Log(version + " is not recognizable by this animation");
        }
    }
    public override void OnEndAnimation() { }
    #endregion
}
