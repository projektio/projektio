﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

public class TwistingSlashAnimationEvents : WeaponAnimationEvents
{
    #region Methods
    public override void OnBeginAnimation() { }

    public override void OnInsideAnimation(string version) { }
    public override void OnEndAnimation() { }
    #endregion
}