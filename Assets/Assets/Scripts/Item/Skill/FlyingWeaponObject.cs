﻿using UnityEngine;
using System.Collections;

public class FlyingWeaponObject : MonoBehaviour
{
    #region Variables

    [SerializeField]
    private bool hitEffectOnlyOnCharacter = false;

    [SerializeField]
    private GameObject hitEffect;

    [SerializeField]
    private bool destroyOnCollision = true;

    [SerializeField]
    private int damage;

    [SerializeField]
    private bool collideWithItems = false;

    public AudioClip HitClip;
    #endregion

    #region Properties
    public Character Owner { get; set; } //Who shoots this object
    #endregion

    #region Functions
    public void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.tag);
        if (!collideWithItems && other.tag.StartsWith(Tags.item))
        {
            return;
        }
        if (other.gameObject.tag == Tags.character)
        {
            Character c = other.gameObject.GetComponent<Character>();
            c.Stats.HealthPoints -= (Owner.Stats.WizardryDamage + damage);
        }
        if (HitClip)
        {
            AudioSource.PlayClipAtPoint(HitClip, transform.position);
        }
        if (hitEffect)
        {
            if (!hitEffectOnlyOnCharacter || (hitEffectOnlyOnCharacter && other.tag.StartsWith(Tags.character)))
            {
                Instantiate(hitEffect, transform.position, transform.rotation);
            }
        }
        if (destroyOnCollision)
        {
            Destroy(this.gameObject);
        }
    }
    #endregion
}