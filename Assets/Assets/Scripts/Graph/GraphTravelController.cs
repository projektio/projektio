﻿using System.Collections;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;

namespace Graph
{
    public class GraphTravelController : MonoBehaviour, IGraphEventListener
    {
        #region Variables
        private List<Edge> activeEdges = null;

        [SerializeField]
        private float infoLabelDuration = 5f;
        [SerializeField]
        private int minDistanceToShow = 100;

        //References
        private Player player;
        private GameController gameController;
        private NGUIWindowManager nguiWindowManager;
        #endregion

        #region Functions

        void Awake()
        {
            player = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<Player>();
            //Here we want to populate enemies
            gameController = GameObject.FindGameObjectWithTag(Tags.gameController)
                .GetComponent<GameController>();

            nguiWindowManager = GameObject.FindGameObjectWithTag(Tags.gameController)
                .GetComponent<NGUIWindowManager>();
        }

        public void Update()
        {
            if (activeEdges.Count > 0)
            {
                int dist = (int)(player.transform.position - activeEdges[0].transform.position).magnitude;
                for (int i = 0; i < activeEdges.Count; i++)
                {
                    dist = Mathf.Min(dist, (int)(player.transform.position - activeEdges[i].transform.position).magnitude);
                }
                if (dist >= minDistanceToShow)
                    nguiWindowManager.DistanceLabel.text = "Distance: " + dist;
                else
                    nguiWindowManager.DistanceLabel.text = "";
            }
            else
            {
                nguiWindowManager.DistanceLabel.text = "";
            }
        }

        public void OnRootNodeEnter(Node root)
        {
            foreach (Edge e in root.EdgeList)
            {
                e.gameObject.SetActive(true);
            }
            activeEdges = root.EdgeList;
            for (int i = 0; i < root.weaponList.Count; i++)
            {
                Weapon weapon = (Weapon)Instantiate(root.weaponList[i]);
                weapon.gameObject.SetActive(false);
                gameController.AddWeaponToPlayer(weapon);
                StartCoroutine(DisplayInfoLabel("Weapon: " + weapon.Name + " added"));
            }
            for (int i = 0; i < root.skillList.Count; i++)
            {
                Skill skill = (Skill)Instantiate(root.skillList[i]);
                gameController.AddWeaponToPlayer(skill);
                StartCoroutine(DisplayInfoLabel("Skill: " + skill.Name + " added"));
            }
        }

        public void OnEdgeEnter(Edge edge)
        {
            //Disable all active edges
            foreach (Edge e in activeEdges)
            {
                e.gameObject.SetActive(false);
            }
            activeEdges.Clear();

            gameController.PopulateEnemies(edge);

            //Here is code for giving surprices to our hero :) (it can change to balance difficulty level)
            for (int i = 0; i < edge.DestinationNode.weaponList.Count; i++)
            {
                Weapon weapon = (Weapon)Instantiate(edge.DestinationNode.weaponList[i]);
                weapon.gameObject.SetActive(false);
                gameController.AddWeaponToPlayer(weapon);
                StartCoroutine(DisplayInfoLabel("Weapon: " + weapon.Name + " added"));
            }
            for (int i = 0; i < edge.DestinationNode.skillList.Count; i++)
            {
                Skill skill = (Skill)Instantiate(edge.DestinationNode.skillList[i]);
                gameController.AddWeaponToPlayer(skill);
                StartCoroutine(DisplayInfoLabel("Skill: " + skill.Name + " added"));
            }
        }

        public void OnEdgeAccomplish(Edge edge)
        {
            activeEdges = edge.DestinationNode.EdgeList;
            foreach (Edge e in activeEdges)
            {
                e.gameObject.SetActive(true);
            }
        }

        public void OnLeafAccomplish(Leaf leaf)
        {
            //TODO it should be changed (for example add some sound effects etc.)
            nguiWindowManager.SubmitScoreWindow.Show();

            //Change time inside SubmitScoreWindow
            nguiWindowManager.SubmitScoreWindow.TimeLabel.text = gameController
                .GetComponent<HUD>().GameTime.ToString("0.0");
        }

        //Unity doesn't support multithreading, but it supports cooperative multithreading.
        //In Unity it's called coroutines.
        private IEnumerator DisplayInfoLabel(string text)
        {
            UILabel label = nguiWindowManager.InfoLabel;
            label.text = text;
            yield return new WaitForSeconds(infoLabelDuration);
            label.text = "";
            yield return null;
        }
        #endregion
    }
}