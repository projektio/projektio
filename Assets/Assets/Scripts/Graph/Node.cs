﻿//Norbert Gregorek

using UnityEngine;
using System.Collections.Generic;

namespace Graph
{
    [System.Serializable]
    public class Node : MonoBehaviour
    {
        #region Variables
        //Skills and weapons for our player
        [SerializeField]
        public List<Weapon> weaponList;
        [SerializeField]
        public List<Skill> skillList;
        #endregion

        #region Properties
        public int Depth { get; set; }
        public List<Edge> EdgeList { get; set; }
        #endregion

        #region Functions
        public Node()
        {
            Depth = -1;
            EdgeList = new List<Edge>();
        }
        #endregion

    }
}