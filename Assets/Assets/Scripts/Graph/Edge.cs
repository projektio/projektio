﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

namespace Graph
{
    public class Edge : MonoBehaviour
    {
        #region Variables
        [SerializeField]
        private Node sourceNode;
        [SerializeField]
        private Node destinationNode;
        #endregion

        #region Properties
        public Node DestinationNode
        {
            get { return destinationNode; }
        }

        public Node SourceNode
        {
            get { return sourceNode; }
        }
        #endregion

        #region Functions
        public void Awake()
        {
            //Just add edge to the source node
            SourceNode.EdgeList.Add(this);
        }
        #endregion
    }
}