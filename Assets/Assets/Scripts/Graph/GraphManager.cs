﻿//Norbert Gregorek

using UnityEngine;
using System.Collections.Generic;
using Extension;
using System.Linq;

namespace Graph
{
    /* This class oversees how gameplay looks like.
     * It controls workflow in our regular graph.
     * */
    public class GraphManager : MonoBehaviour
    {
        enum GraphState
        {
            EDGE_SELECTED,
            EDGE_UNSELECTED,
            NO_MORE_EDGES,
        };

        #region Variables
        [SerializeField]
        //It should be a root at the beginning
        private Node root;
        [SerializeField]
        //It indicates number of different weapons
        private int pathLength;

        private GraphState state;
        private List<IGraphEventListener> graphListeners;
        #endregion

        #region Functions
        public void Awake()
        {
            InitializeGraph(root, null);
            state = GraphState.EDGE_UNSELECTED;
            graphListeners = this.GetInterfaces<IGraphEventListener>().ToList();

            foreach (IGraphEventListener listener in graphListeners)
            {
                listener.OnRootNodeEnter(root);
            }
        }

        public void OnEdgeEnter(Edge edge)
        {
            if (state == GraphState.EDGE_UNSELECTED)
            {
                //Update some data
                state = GraphState.EDGE_SELECTED;
                root = edge.DestinationNode;

                foreach (IGraphEventListener listener in graphListeners)
                {
                    listener.OnEdgeEnter(edge);
                }
            }
        }

        public void OnEdgeAccomplish(Edge edge)
        {
            if (state == GraphState.EDGE_SELECTED)
            {
                foreach (IGraphEventListener listener in graphListeners)
                {
                    if (edge.DestinationNode.GetComponent<Leaf>() == null)
                    {
                        listener.OnEdgeAccomplish(edge);
                    }
                    else
                    {
                        listener.OnLeafAccomplish(edge.DestinationNode.GetComponent<Leaf>());
                    }
                }
                state = GraphState.EDGE_UNSELECTED;
            }
        }

        public void OnLeafAccomplish(Leaf leaf)
        {
            state = GraphState.NO_MORE_EDGES;
            foreach (IGraphEventListener listener in graphListeners)
            {
                listener.OnLeafAccomplish(leaf);
            }
        }

        
        //Here we initialize our graph, it uses DFS algorithm
        public void InitializeGraph(Node node, Node previousNode)
        {
            if (node == root)
            {
                node.Depth = 0;
            }
            else
            {
                if (node.Depth == -1)
                {
                    node.Depth = previousNode.Depth + 1;
                }
                else if (node.Depth != previousNode.Depth + 1)
                {
                    Debug.LogError("This graph is not regular. Vertex " + node.name
                        +" has at least two different routes that have different length.");
                    return;
                }
            }
            //Call this function recursively
            foreach (Edge e in node.EdgeList)
            {
                e.gameObject.SetActive(false);
                InitializeGraph(e.DestinationNode, node);
            }
        }
        #endregion
    }
}