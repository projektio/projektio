﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

namespace Graph
{
    public interface IGraphEventListener
    {
        void OnRootNodeEnter(Node root);
        void OnEdgeEnter(Edge edge);
        void OnEdgeAccomplish(Edge edge);
        void OnLeafAccomplish(Leaf leaf);
    }
}