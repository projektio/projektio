﻿//Norbert Gregorek

using UnityEngine;
using System.Collections;

namespace Graph
{
    //Control material of a script and react on collision
    public class EdgeController : MonoBehaviour
    {
        [SerializeField]
        private float frequency = 2f;

        private Material material;
        private Edge edge;

        public void Awake()
        {
            material = renderer.material;
            edge = GetComponent<Edge>();
        }

        public void Update()
        {
            Color c = material.color;
            c.a = Mathf.Clamp(Mathf.Abs(Mathf.Sin(Time.time / frequency)) / 4, 0.5f, 1f);
            c.r = Mathf.Clamp(Mathf.Abs(Mathf.Sin(Time.time / frequency)) / 3, 0.1f, 0.5f);
            material.color = c;
        }

        public void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == Tags.player)
            {
                GraphManager graphManager = GameObject.FindGameObjectWithTag(Tags.graphController)
                    .GetComponent<GraphManager>();

                graphManager.OnEdgeEnter(edge);
            }
        }
    }
}