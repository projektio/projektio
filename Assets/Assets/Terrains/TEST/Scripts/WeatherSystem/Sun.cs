﻿using UnityEngine;
using System.Collections;

public class Sun : MonoBehaviour {
	public float minLightBrightness = 0;
	public float maxLightBrightness = 1;
	public float minFlareBrightness = 0;
	public float maxFlareBrightness = 1;
}
