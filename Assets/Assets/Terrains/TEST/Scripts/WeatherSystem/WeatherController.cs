﻿using UnityEngine;
using System.Collections;

public class WeatherController : MonoBehaviour {
	public enum TimeOfDay {
		Idle,
		SunRise,
		SunSet
	}
	[SerializeField]
	protected Transform sun;
	private Sun sunScript;	
	[SerializeField]
	protected float dayCycleInMinutes = 1;

	[SerializeField]
	protected float sunRise;
	[SerializeField]
	protected float sunSet;
	[SerializeField]
	protected float skyboxBlendModifier;

	[SerializeField]
	protected Color ambLightMax;
	[SerializeField]
	protected Color ambLightMin;

	private float dayCycleInSeconds;
	private const float second = 1;
	private const float minute = 60 * second;
	private const float hour = 60 * minute;
	private const float day = 24 * hour;

	private const float degrees_per_second = 360 / day;

	private float degreeRotation;
	private float timeOfDay;

	private TimeOfDay tod;
	private float noonTime;
	private float morningLength;
	private float eveningLength;

	// Use this for initialization
	void Start() {
		tod = TimeOfDay.Idle;
		dayCycleInSeconds = dayCycleInMinutes * minute;
		sunScript = sun.GetComponent<Sun>();
		timeOfDay = 0;
		degreeRotation = degrees_per_second * day / dayCycleInSeconds;
		RenderSettings.skybox.SetFloat("_Blend", 0);

		sunRise *= dayCycleInSeconds;
		sunSet *= dayCycleInSeconds;
		noonTime = dayCycleInSeconds / 2;
		morningLength = noonTime - sunRise;
		eveningLength = morningLength;

		RenderSettings.ambientLight = ambLightMin;
		sun.GetComponent<Light>().intensity = sunScript.minLightBrightness;
	}
	
	// Update is called once per frame
	void Update() {
		sun.Rotate(new Vector3(degreeRotation, 0, 0) * Time.deltaTime);
		timeOfDay += Time.deltaTime;
		if (timeOfDay > dayCycleInSeconds)
			timeOfDay -= dayCycleInSeconds;

		if (timeOfDay > sunRise && timeOfDay < noonTime) {
			AdjustLighting(true);
		} else if(timeOfDay > sunSet) {
			AdjustLighting(false);
		}

		if (timeOfDay > sunRise && timeOfDay < sunSet && RenderSettings.skybox.GetFloat ("_Blend") < 1.0f) {
			tod = TimeOfDay.SunRise;
			BlendSkybox ();
		} else if (timeOfDay > sunSet && RenderSettings.skybox.GetFloat ("_Blend") > 0.0f) {
			tod = TimeOfDay.SunSet;
			BlendSkybox ();
		} else {
			tod = TimeOfDay.Idle;
		}
	}

	private void BlendSkybox() {
		float fraction = 0.0f;
		switch (tod) {
		case TimeOfDay.SunRise:
			fraction = (timeOfDay - sunRise) / dayCycleInSeconds * skyboxBlendModifier;
			break;
		case TimeOfDay.SunSet:
			fraction = (timeOfDay - sunSet) / dayCycleInSeconds * skyboxBlendModifier;
			fraction = 1 - fraction;
			break;
		}
		RenderSettings.skybox.SetFloat("_Blend", fraction);
	}

	private void AdjustLighting(bool brighten) {
		float fraction = 0;
		if (brighten) {
			fraction = (timeOfDay - sunRise) / morningLength;
		} else {
			fraction = (timeOfDay - sunSet) / eveningLength;
			fraction = 1 - fraction;
		}
		fraction = Mathf.Min(fraction, 1.0f);
		fraction = Mathf.Max(fraction, 0.0f);
		RenderSettings.ambientLight = (ambLightMax - ambLightMin) * fraction + ambLightMin;
		sun.GetComponent<Light>().intensity = (sunScript.maxLightBrightness - sunScript.minLightBrightness) * fraction + sunScript.minLightBrightness;

	}
}
